\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {subsection}{\numberline {1.1}History of Question Answering}{2}
\contentsline {subsection}{\numberline {1.2}Example Use Cases}{3}
\contentsline {section}{\numberline {2}Approaches to Question Answering}{3}
\contentsline {subsection}{\numberline {2.1}Knowledge-based Question Answering}{3}
\contentsline {subsubsection}{\numberline {2.1.1}Rule-based Methods}{3}
\contentsline {subsubsection}{\numberline {2.1.2}Supervised Methods}{4}
\contentsline {subsubsection}{\numberline {2.1.3}Semi-supervised Methods}{4}
\contentsline {subsection}{\numberline {2.2}IR-based Question Answering}{4}
\contentsline {subsubsection}{\numberline {2.2.1}Question Processing}{4}
\contentsline {subsubsection}{\numberline {2.2.2}Document/Passage Selection}{5}
\contentsline {subsubsection}{\numberline {2.2.3}Answer Extraction}{5}
\contentsline {subsection}{\numberline {2.3}Machine Comprehension Oriented Question Answering}{5}
\contentsline {section}{\numberline {3}Evaluation of QA Systems}{7}
\contentsline {section}{\numberline {4}Question Answering in Non-English Languages}{7}
\contentsline {section}{\numberline {5}Popular Datasets, Success Rates and Frequently Used Databases}{8}
\contentsline {subsection}{\numberline {5.1}Popular Datasets and Success Rates}{8}
\contentsline {subsubsection}{\numberline {5.1.1}In Knowledge-based Question Answering Systems}{8}
\contentsline {subsubsection}{\numberline {5.1.2}In IR-based Question Answering Systems}{8}
\contentsline {subsubsection}{\numberline {5.1.3}In Machine Comprehension Oriented Answering Systems}{9}
\contentsline {subsection}{\numberline {5.2}Frequently Used Databases}{9}
\contentsline {section}{\numberline {6}Systems and Tools Currently Used}{9}
\contentsline {section}{\numberline {7}Conclusion}{10}
